# coolfetch

a fetch tool made in C

![Ubuntu](ubuntu.png)
![Arch](arch.png)
![Debian](debian.png)

it maybe even called tankfetch!

## Quick Start

GNU/Linux:
```console
$ make -f Makefile.gnu

$ ./coolfetch
```

Debian based distros(including debian):
```console
$ make -f Makefile.debi

$ ./coolfetch
```

Musl-based distros/arch linux!
```console
$ make -f Makefile.musl

$ ./coolfetch
```

BSD or anything else that has /etc/os-release
```console
$ make -f Makefile.bsd

$ ./coolfetch
```

## Coming soon stuff
~~- Logos/Icons for distros~~ Done

- Config files

~~- Move logos!~~ Done
